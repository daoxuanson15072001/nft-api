import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config } from './config';
import Moralis from 'moralis';
import * as bodyParser from 'body-parser';

require('dotenv').config();
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.enableCors();
   await Moralis.start({
     apiKey: config.moralis.apiKey,
   }).then(async() => {
    await app.listen(3000);
   });
}
bootstrap();
