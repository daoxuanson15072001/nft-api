import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './app/auth/auth.module';
import { dataSourceOptions } from './database/data-source';
import { ConfigModule } from '@nestjs/config';
import { NftModule } from './app/nft/nft.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksService } from './helpers/schedule';
import { TransactionModule } from './app/transaction/transaction.module';
import { S3UploadModule } from './app/s3-upload/s3-upload.module';
import { HolderModule } from './app/holder/holder.module';

require('dotenv').config();
@Module({
  imports: [
    TypeOrmModule.forRoot(dataSourceOptions),
    AuthModule,
    NftModule,
    TransactionModule,
    S3UploadModule,
    ConfigModule.forRoot({ isGlobal: true }),
    ScheduleModule.forRoot(),
    HolderModule
  ],
  controllers: [AppController],
  providers: [AppService, TasksService],
})
export class AppModule {}
