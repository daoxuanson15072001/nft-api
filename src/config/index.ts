require('dotenv').config();
const InputDataDecoder = require('ethereum-input-data-decoder');
const abi = require('../../ABI.json');
export const config = {
  UPLOAD: {
    S3_REGION: process.env.S3_REGION,
    S3_ACCESS_KEY: process.env.S3_ACCESS_KEY,
    S3_SECRET_KEY: process.env.S3_SECRET_KEY,
    S3_BUCKET: process.env.S3_BUCKET,
    S3_DOMAIN: process.env.S3_DOMAIN,
    MAX_FILES: process.env.MAX_FILES,
  },
  moralis: {
    apiKey: process.env.MORALIS_API_KEY,
    contractAddress: process.env.CONTRACT_ADDRESS,
    decoder: new InputDataDecoder(abi),
    abi: abi,
  },
  bsc: {
    apiKey: process.env.BSC_API_KEY,
    apiDomain: process.env.BSC_API_DOMAIN,
  },
  MYSQL: {
    HOST: process.env.MYSQL_HOST,
    PORT: Number(process.env.MYSQL_PORT) || 3306,
    USER: process.env.MYSQL_USER,
    PASSWORD: process.env.MYSQL_PASS,
    DB_NAME: process.env.MYSQL_DB_NAME,
  },
};
