import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateTablePrice1681893676094 implements MigrationInterface {
    name = 'UpdateTablePrice1681893676094'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` text NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` bigint UNSIGNED NOT NULL DEFAULT '0'`);
    }

}
