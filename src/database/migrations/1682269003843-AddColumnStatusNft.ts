import { MigrationInterface, QueryRunner } from "typeorm";

export class AddColumnStatusNft1682269003843 implements MigrationInterface {
    name = 'AddColumnStatusNft1682269003843'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`description\` text NULL`);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`status\` enum ('1', '0') NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`status\``);
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`description\``);
    }

}
