import { InitDB1680062965475 } from './1680062965475-InitDB';
import { CreateTableNft1680767673360 } from './1680767673360-CreateTableNft';
import { AddColumnOwnerAddress1681393267001 } from './1681393267001-AddColumnOwnerAddress';
import { AddMetaData1681652242820 } from './1681652242820-AddMetaData';
import { CreateTableTransaction1681668050626 } from './1681668050626-CreateTableTransaction';
import { UpdatePriceNFT1681669505076 } from './1681669505076-UpdatePriceNFT';
import { UpdateImageNFT1681838324235 } from './1681838324235-UpdateImageNFT';
import { UpdateTablePrice1681893676094 } from './1681893676094-UpdateTablePrice';
import { AddColumnStatusNft1682269003843 } from './1682269003843-AddColumnStatusNft';
import { CreateTableHolder1683351026179 } from './1683351026179-CreateTableHolder';
import { addColumnThumbnail1683645272703 } from './1683645272703-addColumnThumbnail';



export const DefaultMigrations = [
  InitDB1680062965475,
  CreateTableNft1680767673360,
  AddColumnOwnerAddress1681393267001,
  AddMetaData1681652242820,
  CreateTableTransaction1681668050626,
  UpdatePriceNFT1681669505076,
  UpdateImageNFT1681838324235,
  UpdateTablePrice1681893676094,
  AddColumnStatusNft1682269003843,
  CreateTableHolder1683351026179,
  addColumnThumbnail1683645272703,
];
