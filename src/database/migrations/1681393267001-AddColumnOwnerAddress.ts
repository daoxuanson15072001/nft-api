import { MigrationInterface, QueryRunner } from "typeorm";

export class AddColumnOwnerAddress1681393267001 implements MigrationInterface {
    name = 'AddColumnOwnerAddress1681393267001'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`owner_address\` varchar(255) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`owner_address\``);
    }

}
