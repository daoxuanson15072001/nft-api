import { MigrationInterface, QueryRunner } from "typeorm";

export class AddMetaData1681652242820 implements MigrationInterface {
    name = 'AddMetaData1681652242820'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`image\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`nft_name\` varchar(255) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`nft_name\``);
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`image\``);
    }

}
