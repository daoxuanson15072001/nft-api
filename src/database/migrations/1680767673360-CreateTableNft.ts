import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTableNft1680767673360 implements MigrationInterface {
    name = 'CreateTableNft1680767673360'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`nft\` (\`token_id\` varchar(255) NOT NULL, \`token_address\` varchar(255) NOT NULL, \`amount\` int NOT NULL, \`token_hash\` varchar(255) NOT NULL, \`block_number_minted\` int NOT NULL, \`contract_type\` varchar(255) NOT NULL, \`name\` varchar(255) NOT NULL, \`symbol\` varchar(255) NOT NULL, \`token_uri\` varchar(255) NULL, \`metadata\` text NULL, \`normalized_metadata\` json NULL, \`last_token_uri_sync\` varchar(255) NOT NULL, \`last_metadata_sync\` varchar(255) NULL, \`minter_address\` varchar(255) NOT NULL, \`possible_spam\` tinyint NOT NULL DEFAULT 0, \`created_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`token_id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`nft\``);
    }

}
