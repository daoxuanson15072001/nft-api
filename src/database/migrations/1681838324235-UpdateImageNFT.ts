import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateImageNFT1681838324235 implements MigrationInterface {
    name = 'UpdateImageNFT1681838324235'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`image\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`image\` text NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`image\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`image\` varchar(255) NULL`);
    }

}
