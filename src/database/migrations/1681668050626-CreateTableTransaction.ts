import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTableTransaction1681668050626 implements MigrationInterface {
    name = 'CreateTableTransaction1681668050626'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`transaction\` (\`transaction_hash\` varchar(255) NOT NULL, \`block_number\` varchar(255) NOT NULL, \`block_timestamp\` datetime NULL, \`block_hash\` varchar(255) NOT NULL, \`transaction_index\` int NOT NULL, \`log_index\` int NOT NULL, \`value\` varchar(255) NOT NULL, \`contract_type\` varchar(255) NOT NULL, \`transaction_type\` varchar(255) NOT NULL, \`token_id\` varchar(255) NOT NULL, \`token_address\` varchar(255) NOT NULL, \`from_address\` varchar(255) NOT NULL, \`to_address\` varchar(255) NOT NULL, \`amount\` varchar(255) NOT NULL, \`verified\` int NOT NULL DEFAULT '1', \`operator\` varchar(255) NULL, \`method\` varchar(255) NULL, \`created_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`transaction_hash\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`transaction\``);
    }

}
