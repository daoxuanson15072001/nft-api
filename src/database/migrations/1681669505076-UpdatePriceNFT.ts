import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdatePriceNFT1681669505076 implements MigrationInterface {
    name = 'UpdatePriceNFT1681669505076'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` bigint UNSIGNED NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` int NULL`);
    }

}
