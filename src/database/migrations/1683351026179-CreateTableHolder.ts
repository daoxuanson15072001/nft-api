import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTableHolder1683351026179 implements MigrationInterface {
    name = 'CreateTableHolder1683351026179'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`holder\` (\`address\` varchar(255) NOT NULL, \`balance\` bigint UNSIGNED NULL, \`full_name\` varchar(255) NULL, \`email\` varchar(255) NULL, \`mobile\` varchar(20) NULL, \`avatar\` varchar(255) NULL, \`instagram\` varchar(255) NULL, \`status\` tinyint NOT NULL COMMENT '0: Inactive, 1: Active.' DEFAULT '1', \`created_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), PRIMARY KEY (\`address\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` bigint UNSIGNED NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`price\``);
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`price\` text NULL`);
        await queryRunner.query(`DROP TABLE \`holder\``);
    }

}
