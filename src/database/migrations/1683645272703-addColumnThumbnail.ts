import { MigrationInterface, QueryRunner } from "typeorm";

export class addColumnThumbnail1683645272703 implements MigrationInterface {
    name = 'addColumnThumbnail1683645272703'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` ADD \`thumbnail\` varchar(255) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`nft\` DROP COLUMN \`thumbnail\``);
    }

}
