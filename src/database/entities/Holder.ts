import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
@Entity('holder')
export default class Holder {
  @PrimaryColumn({ name: 'address', type: 'varchar', length: 255 })
  address: number;

  @Column({ name: 'balance', type: 'bigint', unsigned: true, nullable: true })
  balance: number;

  @Column({ name: 'full_name', type: 'varchar', length: 255, nullable: true })
  fullName: string | null;

  @Column({
    name: 'email',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  email: string | null;

  @Column({
    name: 'mobile',
    type: 'varchar',
    length: 20,
    nullable: true,
  })
  mobile: string | null;

  @Column({ name: 'avatar', type: 'varchar', length: 255, nullable: true })
  avatar: string | null;

  @Column({ name: 'instagram', type: 'varchar', length: 255, nullable: true })
  instagram: string | null;

  @Column({
    name: 'status',
    type: 'tinyint',
    default: 1,
    comment: '0: Inactive, 1: Active.',
  })
  status: number;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', nullable: true })
  updatedAt: Date | null;
}
