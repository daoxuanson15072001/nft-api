import Holder from './Holder';
import Nft from './Nft';
import Room from './Room';
import Transaction from './Transaction';
import User from './User';
import { UserRoom } from './UserRoom';

export const DefaultEntities = [Room, User, UserRoom, Nft, Transaction, Holder];
