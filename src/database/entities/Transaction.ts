import {
  Column,
  Entity,
  UpdateDateColumn,
  CreateDateColumn,
  PrimaryColumn,
} from 'typeorm';

@Entity('transaction')
export default class Transaction {
  @PrimaryColumn({ name: 'transaction_hash', type: 'varchar', length: 255 })
  transactionHash: string;

  @Column({ name: 'block_number', type: 'varchar', length: 255 })
  blockNumber: string;

  @Column({
    name: 'block_timestamp',
    nullable: true,
  })
  blockTimeStamp: Date;

  @Column({ name: 'block_hash', type: 'varchar', length: 255 })
  blockHash: string;

  @Column({ name: 'transaction_index', type: 'int' })
  transactionIndex: number;

  @Column({ name: 'log_index', type: 'int' })
  logIndex: number;

  @Column({ name: 'value', type: 'varchar', length: 255 })
  value: string;

  @Column({ name: 'contract_type', type: 'varchar', length: 255 })
  contractType: string;

  @Column({ name: 'transaction_type', type: 'varchar', length: 255 })
  transactionType: string;

  @Column({ name: 'token_id', type: 'varchar', length: 255 })
  tokenId: string;

  @Column({ name: 'token_address', type: 'varchar', length: 255 })
  tokenAddress: string;

  @Column({ name: 'from_address', type: 'varchar', length: 255 })
  fromAddress: string;

  @Column({ name: 'to_address', type: 'varchar', length: 255 })
  toAddress: string;

  @Column({ name: 'amount', type: 'varchar', length: 255 })
  amount: string;

  @Column({ name: 'verified', type: 'int', default:1 })
  verified: number;

  @Column({ name: 'operator', type: 'varchar', length: 255, nullable: true })
  operator: string;

  @Column({ name: 'method', type: 'varchar', length: 255, nullable: true })
  method: string;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', nullable: true })
  updatedAt: Date | null;
}
