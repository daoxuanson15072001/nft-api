import { LiteralObject } from '@nestjs/common';
import { NftStatus } from '../../enums/enum';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('nft')
export default class Nft {
  @PrimaryColumn({ name: 'token_id', type: 'varchar', length: 255 })
  tokenId: string;

  @Column({ name: 'token_address', type: 'varchar', length: 255 })
  tokenAddress: string;

  @Column({ name: 'amount', type: 'int' })
  amount: string;

  @Column({ name: 'token_hash', type: 'varchar', length: 255 })
  tokenHash: string;

  @Column({ name: 'block_number_minted', type: 'int' })
  blockNumberMinted: string;

  @Column({ name: 'contract_type', type: 'varchar', length: 255 })
  contractType: string;

  @Column({ name: 'name', type: 'varchar', length: 255 })
  name: string;

  @Column({ name: 'symbol', type: 'varchar', length: 255 })
  symbol: string;

  @Column({ name: 'token_uri', type: 'varchar', length: 255, nullable: true })
  tokenUri: string;

  @Column({ name: 'metadata', type: 'text', nullable: true })
  metadata: string;

  @Column({
    name: 'normalized_metadata',
    type: 'json',
    nullable: true,
  })
  normalizedMetadata: LiteralObject;

  @Column({ name: 'last_token_uri_sync', type: 'varchar', length: 255 })
  lastTokenUriSync: string;

  @Column({
    name: 'last_metadata_sync',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  lastMetadataSync: string;

  @Column({ name: 'minter_address', type: 'varchar', length: 255 })
  mintedAddress: string;

  @Column({ name: 'possible_spam', type: 'boolean', default: false })
  possibleSpam: boolean;

  @Column({
    name: 'owner_address',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  ownerAddress: string;

  @Column({
    name: 'image',
    type: 'text',
    nullable: true,
  })
  image: string;
  @Column({
    name: 'price',
    type: 'bigint',
    unsigned: true,
    nullable: true,
  })
  price: number;
  @Column({
    name: 'nft_name',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  nftName: string;

  @Column({
    name: 'description',
    type: 'text',
    nullable: true,
  })
  description: string;

  @Column({
    name: 'status',
    type: 'enum',
    enum: NftStatus,
    default: NftStatus.NOT_SALE,
  })
  status: number;

  @Column({
    name: 'thumbnail',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  thumbnail: string;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp', nullable: true })
  updatedAt: Date | null;
}
