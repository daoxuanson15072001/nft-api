export interface InputData {
  to: string;
  tokenId: number;
  tokenImage: string;
  price: number;
  name: string;
}
