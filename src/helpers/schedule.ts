import { getRepository } from 'typeorm';
import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { config } from 'src/config';
import Moralis from 'moralis';
import Nft from 'src/database/entities/Nft';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  @Cron(CronExpression.EVERY_DAY_AT_1AM, {
    timeZone: 'Asia/Bangkok',
  })
  async asyncNftByContract() {
    try {
      this.logger.log('start async contract with mysql');
      const response = await Moralis.EvmApi.nft.getContractNFTs({
        chain: '0x61',
        format: 'decimal',
        normalizeMetadata: true,
        mediaItems: true,
        address: config.moralis.contractAddress,
      });
      const listNftByContract = response.raw.result.map((item) => ({
        tokenAddress: item.token_address,
        tokenId: item.token_id,
        amount: item.amount,
        tokenHash: item.token_hash,
        blockNumberMinted: item.block_number_minted,
        contractType: item.contract_type,
        name: item.name,
        symbol: item.symbol,
        tokenUri: item.token_uri,
        metadata: item.metadata,
        lastTokenUriSync: item.last_token_uri_sync,
        lastMetaDataSync: item.last_metadata_sync,
        mintedAddress: item.minter_address,
        normalizedMetadata: item.normalized_metadata,
        possibleSpam: item.possible_spam,
      }));
      await getRepository(Nft).save(listNftByContract);
      this.logger.log('save Nft success');
    } catch (e) {
      console.error(e);
    }
  }
}
