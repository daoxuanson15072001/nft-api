import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { query } from 'express';

@Controller('transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService , 
    ) {}

  @Get('/refresh')
  async create() {
    return await this.transactionService.refreshTransaction();
  }

  @Get('/list')
  async getListTransaction(@Query() query) {
    return await this.transactionService.getListTransaction(query);
  }

  @Get('/list/:tokenId')
  async getListTransactionByTokenId(@Param('tokenId') tokenId, @Query() query) {
    return await this.transactionService.getListTransactionByTokenId(
      tokenId,
      query,
    );
  }

  @Post('/')
  async createTransaction(@Body() body) {
    return await this.transactionService.createTransaction(body);
  }
}
