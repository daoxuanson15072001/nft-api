import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { config } from 'src/config';
import Transaction from 'src/database/entities/Transaction';
import { Repository } from 'typeorm';
import Moralis from 'moralis';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
  ) {}

  async refreshTransaction() {
    try {
      const decoder = config.moralis.decoder;
      let listTransaction = [];
      const transaction = (
        await Moralis.EvmApi.nft.getNFTContractTransfers({
          chain: '0x61',
          format: 'decimal',
          address: config.moralis.contractAddress,
        })
      ).result;

      for (let i = 0; i < transaction.length; i++) {
        const responseTransaction = (
          await Moralis.EvmApi.transaction.getTransaction({
            chain: '0x61',
            transactionHash: transaction[i].transactionHash,
          })
        )?.['jsonResponse'];
        const result = decoder.decodeData(responseTransaction.input) as any;
        listTransaction.push({
          transactionHash: transaction[i].transactionHash,
          blockNumber: responseTransaction.block_number,
          blockTimeStamp: responseTransaction.block_timestamp,
          blockHash: responseTransaction.block_hash,
          transactionIndex: Number(responseTransaction.transaction_index),
          logIndex: transaction[i].logIndex,
          value: responseTransaction.value,
          contractType: transaction[i].contractType,
          transactionType: transaction[i].transactionType,
          tokenId: transaction[i].tokenId,
          tokenAddress: transaction[i].tokenAddress?.['_value'],
          fromAddress: responseTransaction.from_address,
          toAddress: responseTransaction.to_address,
          amount: transaction[i].amount,
          operator: transaction[i].operator,
          method: result.method,
        });
      }
      await this.transactionRepository.save(listTransaction);
      return true;
    } catch (e) {
      console.error(e);
    }
  }
  async getListTransaction(params: any) {
    const queryBuider = this.transactionRepository
      .createQueryBuilder('t')
      .where('1=1');
    if (params?.tokenId) {
      queryBuider.andWhere('t.tokenId = :tokenId', {
        tokenId: params.tokenId,
      });
    }
    if (params?.keyword) {
      queryBuider.andWhere(
        't.transaction_type LIKE :keyword OR t.blockNumber LIKE :keyword',
        {
          keyword: `%${params.keyword}%`,
        },
      );
    }

    if (params.startTime) {
      queryBuider.andWhere('t.blockTimeStamp >= :startTime', {
        startTime: params.startTime,
      });
    }

    if (params.endTime) {
      queryBuider.andWhere('t.blockTimeStamp <= :endTime', {
        endTime: params.endTime,
      });
    }

    return await queryBuider.orderBy('t.blockTimeStamp', 'DESC').getMany();
  }

  async getListTransactionByTokenId(tokenId: string, params: any) {
    const queryBuider = this.transactionRepository
      .createQueryBuilder('t')
      .where('t.tokenId = :tokenId', {
        tokenId: tokenId,
      });

    if (params?.keyword) {
      queryBuider.andWhere(
        't.transaction_type LIKE :keyword OR t.blockNumber LIKE :keyword',
        {
          keyword: `%${params.keyword}%`,
        },
      );
    }

    if (params.startTime) {
      queryBuider.andWhere('t.blockTimeStamp >= :startTime', {
        startTime: params.startTime,
      });
    }

    if (params.endTime) {
      queryBuider.andWhere('t.blockTimeStamp <= :endTime', {
        endTime: params.endTime,
      });
    }

    return await queryBuider.orderBy('t.blockTimeStamp', 'DESC').getMany();
  }

  async createTransaction(params) {
    return await this.transactionRepository.save({ ...params });
  }

  
}
