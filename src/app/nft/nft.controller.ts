import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Put,
} from '@nestjs/common';
import { NftService } from './nft.service';
import { CreateNftDto } from './dto/create-nft.dto';
import { assignPaging } from 'src/helpers';

@Controller('nft')
export class NftController {
  constructor(private readonly nftService: NftService) {}

  @Get('/create')
  async create() {
    return await this.nftService.create();
  }

  @Get('/list')
  async getListNft(@Query() query) {
    return await this.nftService.getListNft(query);
  }

  @Get('/top-holder')
  async getTopHolder(@Query() query) {
    assignPaging(query);
    return this.nftService.getTopHolder(query);
  }
  @Get('/top-nft')
  async getTopNft() {
    return this.nftService.getTopNft();
  }
  @Get('/:tokenId')
  async getDetailNft(@Param('tokenId') tokenId) {
    return await this.nftService.getDetailNft(String(tokenId));
  }

  @Post('/update')
  async updateOwner(@Body() body) {
    return await this.nftService.updateOwner(body);
  }

  @Post('/listing')
  async listingNft(@Body() body) {
    return await this.nftService.listingNft(body);
  }

  @Put('/cancel-sell/:tokenId')
  async cancelSell(@Param('tokenId') tokenId) {
    return await this.nftService.cancelSell(tokenId);
  }

  @Post('/set-price')
  async setPrice(@Body()body) {
    return await this.nftService.setPrice(body)
  }
}
