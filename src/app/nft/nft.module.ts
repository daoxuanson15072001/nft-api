import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { NftService } from './nft.service';
import { NftController } from './nft.controller';
import Nft from 'src/database/entities/Nft';
import Transaction from 'src/database/entities/Transaction';

@Module({
  imports: [TypeOrmModule.forFeature([Nft,Transaction])],
  controllers: [NftController],
  providers: [NftService]
})
export class NftModule {}
