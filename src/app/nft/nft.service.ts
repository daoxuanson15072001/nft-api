import { hash } from 'bcrypt';
import { Injectable } from '@nestjs/common';
import Moralis from 'moralis';
import { config } from 'src/config';
import { InjectRepository } from '@nestjs/typeorm';
import Nft from 'src/database/entities/Nft';
import { Repository } from 'typeorm';
import { NftStatus } from 'src/enums/enum';
import { QueryDto } from '../shared/dto/query.dto';
import { returnPaging } from 'src/helpers';
import Transaction from 'src/database/entities/Transaction';
const Web3 = require('web3');
const web3 = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545/');

@Injectable()
export class NftService {
  constructor(
    @InjectRepository(Nft) private readonly nftRepository: Repository<Nft>,
    @InjectRepository(Transaction)
    private readonly transactionRepository: Repository<Transaction>,
  ) {}

  async create() {
    try {
      const decoder = config.moralis.decoder;
      let transactionArray = [];
      let listNft = [];
      const contract = new web3.eth.Contract(
        config.moralis.abi,
        config.moralis.contractAddress,
      );
      const functionName =
        'mintNFT(address to,string tokenImage,uint256 price,string name,string description)';
      const lastBlock = await web3.eth.getBlockNumber();
      const url = `${config.bsc.apiDomain}?module=account&action=txlist&address=${config.moralis.contractAddress}&startblock=0&endblock=${lastBlock}&page=1&offset=1000&sort=asc&apikey=${config.bsc.apiKey}`;
      await fetch(url)
        .then((response) => response.json())
        .then((data) => {
          data.result?.forEach((item, index) => {
            if (item.isError == 0 && item.functionName === functionName) {
              transactionArray.push({
                hash: item.hash,
                input: item.input,
                isError: Number(item.isError),
                timeStamp: Number(item.timeStamp),
              });
            }
          });
        })
        .catch((error) => console.error(error));

      let transfers = (
        await Moralis.EvmApi.nft.getNFTContractTransfers({
          chain: '0x61',
          format: 'decimal',
          address: config.moralis.contractAddress,
        })
      ).result;
      let transaction_hash = transfers.reduce((acc, cur) => {
        acc[`${cur.transactionHash}`] = cur;
        return acc;
      }, {});
      for (let i = 0; i < transactionArray.length; i++) {
        const result = decoder.decodeData(transactionArray[i].input) as any;
        const hash = transactionArray[i].hash;
        listNft.push({
          tokenId: transaction_hash?.[hash].tokenId,
          tokenImage: result?.inputs?.[1].toString(),
          price: result?.inputs?.[2].toString(),
          name: result?.inputs?.[3].toString(),
          description: result?.inputs?.[4].toString(),
        });
      }
      listNft = listNft.reduce((acc, cur) => {
        acc[`${cur.tokenId}`] = cur;
        return acc;
      }, {});
      const response = await Moralis.EvmApi.nft.getContractNFTs({
        chain: '0x61',
        format: 'decimal',
        normalizeMetadata: true,
        mediaItems: true,
        address: config.moralis.contractAddress,
      });
      const listNftByContract = await Promise.all(
        response?.['jsonResponse'].result.map(async (item) => ({
          tokenAddress: item.token_address,
          tokenId: item.token_id,
          amount: item.amount,
          tokenHash: item.token_hash,
          blockNumberMinted: item.block_number_minted,
          contractType: item.contract_type,
          name: item.name,
          symbol: item.symbol,
          tokenUri: item.token_uri,
          metadata: item.metadata,
          lastTokenUriSync: item.last_token_uri_sync,
          lastMetaDataSync: item.last_metadata_sync,
          mintedAddress: item.minter_address,
          normalizedMetadata: item.normalized_metadata,
          possibleSpam: item.possible_spam,
          ownerAddress: await contract.methods
            .ownerOf(Number(item.token_id))
            .call(),
          nftName: listNft?.[`${item?.token_id}`].name,
          price: Number(listNft?.[`${item?.token_id}`].price),
          image: listNft?.[`${item?.token_id}`].tokenImage,
          description: listNft?.[`${item?.token_id}`].description,
        })),
      );
      await this.nftRepository.save(listNftByContract);
      return true;
    } catch (e) {
      console.error(e);
    }
  }

  async getListNft(params: any) {
    const queryBuider = this.nftRepository.createQueryBuilder('n');
    if (params.ownerAddress) {
      queryBuider.andWhere(
        'IF(n.ownerAddress IS NULL, n.mintedAddress, n.ownerAddress) = :ownerAddress',
        {
          ownerAddress: params.ownerAddress,
        },
      );
      if (params.status) {
        queryBuider.andWhere('n.status = :status', {
          status: params.status,
        });
      }
    } else {
      queryBuider.andWhere('n.status = :status', { status: NftStatus.LISTING });
    }
    if (params?.keyword) {
      queryBuider.andWhere(
        'n.nftName LIKE :keyword OR n.tokenId LIKE :keyword',
        {
          keyword: `%${params.keyword}%`,
        },
      );
    }
    if (params.priceFrom) {
      queryBuider.andWhere('n.price >= :priceFrom', {
        priceFrom: Number(params.priceFrom),
      });
    }

    if (params.priceTo) {
      queryBuider.andWhere('n.price <= :priceTo', {
        priceTo: Number(params.priceTo),
      });
    }
    if (params.startTime) {
      queryBuider.andWhere('n.createdAt >= :startTime', {
        startTime: params.startTime,
      });
    }

    if (params.endTime) {
      queryBuider.andWhere('n.createdAt <= :endTime', {
        endTime: params.endTime,
      });
    }

    if (params.sortPrice == 1) {
      queryBuider.addOrderBy('n.price', 'DESC');
    } else if (params.sortPrice == 0) {
      queryBuider.addOrderBy('n.price', 'ASC');
    } else {
      queryBuider.addOrderBy('n.createdAt', 'DESC');
    }
    return await queryBuider.getMany();
  }

  async updateOwner({ tokenId, ownerAddress }: any) {
    await this.nftRepository.update(
      { tokenId },
      { ownerAddress, status: NftStatus.NOT_SALE },
    );
    return true;
  }

  async getDetailNft(tokenId: string) {
    return await this.nftRepository.findOne({
      where: {
        tokenId,
      },
    });
  }

  async listingNft(params: { tokenId: string; price }) {
    const { tokenId, price } = params;
    const nft = await this.nftRepository.findOne({ where: { tokenId } });
    if (nft.status === NftStatus.LISTING)
      throw new Error('Sản phẩm đã được đăng bán');
    await this.nftRepository.update(
      { tokenId },
      { status: NftStatus.LISTING, price: Number(price) },
    );

    return true;
  }

  async cancelSell(tokenId: string) {
    await this.nftRepository.update(
      { tokenId },
      { status: NftStatus.NOT_SALE },
    );
    return true;
  }

  async getTopHolder(params: QueryDto) {
    const queryBuider = this.nftRepository
      .createQueryBuilder('n')
      .select(['COUNT(n.tokenId) totalItem'])
      .where('n.status = :status', { status: NftStatus.LISTING })
      .addSelect(
        `IF(n.ownerAddress IS NULL, n.mintedAddress, n.ownerAddress)`,
        'ownerAddress',
      )
      .orderBy('totalItem', 'DESC')
      .groupBy('ownerAddress');
    const count = (await queryBuider.clone().getRawMany()).length;
    const data = await queryBuider
      .skip(params.skip)
      .take(params.take)
      .getRawMany();

    return returnPaging(data, count, params);
  }

  async getTopNft() {
    const qb = this.nftRepository
      .createQueryBuilder('n')
      .select([
        'n.tokenId tokenId',
        'n.image image',
        'n.thumbnail thumbnail',
        'n.mintedAddress mintedAddress',
        'n.ownerAddress ownerAddress',
        'n.price price',
        'n.nftName nftName',
        'n.status status',
      ])
      .leftJoin(Transaction, 't', 't.tokenId = n.tokenId')
      .where(`n.status = ${NftStatus.LISTING}`)
      .addSelect('COUNT(*)', 'count')
      .groupBy('t.tokenId');
    return await qb.getRawMany();
  }

  async setPrice({ price, tokenId }) {
    await this.nftRepository.save({ tokenId, price });
  }
}
