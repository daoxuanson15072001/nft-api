import { assignPaging } from 'src/helpers';

export class CreateNftDto {
  tokenAddress: string;
  tokenId: number;
  amount: number;
  tokenHash: string;
  blockNumberMinted: number;
  contractType: string;
  name: string;
  symbol: string;
  tokenUri?: string;
  metadata?: string;
  lastTokenUriSync?: string;
  lastMetaDataSync?: string;
  mintedAddress: string;
  normalizedMetadata: string;
  possibleSpam: boolean;
}

export class ListTingNftDto {
  tokenId: string;
  price: string;
}

