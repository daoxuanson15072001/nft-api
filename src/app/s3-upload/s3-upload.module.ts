import * as multer from 'multer';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { S3UploadService } from './s3-upload.service';
import { S3UploadController } from './s3-upload.controller';
import { config } from 'src/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import Nft from 'src/database/entities/Nft';
const upload = multer();

@Module({
  imports : [TypeOrmModule.forFeature([Nft])],
  providers: [S3UploadService],
  exports: [S3UploadService],
  controllers: [S3UploadController],
})
export class S3UploadModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(upload.array('files', config.UPLOAD.MAX_FILES))
      .forRoutes(S3UploadController);
  }
}
