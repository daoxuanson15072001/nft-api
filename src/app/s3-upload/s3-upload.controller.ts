import { Body, Controller, Post, UploadedFiles } from '@nestjs/common';
import * as md5 from 'md5';
import { S3UploadService, s3 } from './s3-upload.service';
import { config } from 'src/config';
import { returnLoadMore } from 'src/helpers';
import aws from 'aws-sdk';
import * as fs from 'fs';
import * as ffmpeg from 'fluent-ffmpeg';
import * as ffmpegPath from '@ffmpeg-installer/ffmpeg';
import { first } from 'lodash';
import { exec } from 'child_process';
@Controller('upload')
export class S3UploadController {
  constructor(private readonly s3UploadService: S3UploadService) {}

  @Post('/')
  async uploadFile(@UploadedFiles() files: Array<Express.Multer.File>) {
    const results = [];
    for (const file of files) {
      const arrExt = (file.originalname || '').split('.');
      const originalName = md5(file.originalname);
      const md5Name = arrExt.length
        ? `${originalName}.${arrExt[arrExt.length - 1]}`
        : originalName;
      const fileName = `${Date.now().toString()}-${md5Name}`;
      await this.s3UploadService.putImageToS3(file, fileName);
      results.push(fileName);
    }
    return returnLoadMore(results, {}, { domain: config.UPLOAD.S3_DOMAIN });
  }

  @Post('/thumbnail1s')
  async thumnb1s(@Body('key') key: any) {
    ffmpeg.setFfmpegPath(ffmpegPath.path);
    ffmpeg(`${config.UPLOAD.S3_DOMAIN}/${key}`)
      .setStartTime('00:00:01')
      .setDuration('2')
      .output('video_out.mp4')
      .on('end', function (err) {
        console.log('ok');
        if (!err) {
          console.log('conversion Done');
        }
      })
      .on('error', (err) => console.log('error: ', err))
      .run();
  }

  @Post('/gif')
  async gif(@Body() body) {
    const tokenId = body.tokenId;
    const fileName = body.fileName;
    const fileNameNotExtension = first(fileName.split('.mp'));
    if (!fileNameNotExtension)
      return; /* ------------------------ Handle Logic in my server ----------------------- */
    const done = await new Promise((resolve, reject) => {
      const command = `ffmpeg -ss 0 -i ${config.UPLOAD.S3_DOMAIN}/${fileName} -t 3 \-filter_complex "fps=10,scale=640:-1:flags=lanczos,split [o1] [o2];[o1] palettegen [p]; [o2] fifo [o3];[o3] [p] paletteuse" \-f gif public/${fileNameNotExtension}.gif`;
      
      exec(command, (error, stdout, stderr) => {
        if (error) {
          console.log(`Lỗi khi thực thi lệnh`);
          resolve(false);
          return;
        }
        resolve(true);
      });
    });

    if (done) {
      await this.s3UploadService.handleFileGif(fileNameNotExtension,tokenId);
    }
  }
}
