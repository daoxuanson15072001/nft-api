import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import * as Sharp from 'sharp';
import { config } from 'src/config';
import * as fs from 'fs';
import { InjectRepository } from '@nestjs/typeorm';
import Nft from 'src/database/entities/Nft';
import { Repository } from 'typeorm';

export const s3 = new AWS.S3({
  secretAccessKey: config.UPLOAD.S3_SECRET_KEY,
  accessKeyId: config.UPLOAD.S3_ACCESS_KEY,
  region: config.UPLOAD.S3_REGION,
});
@Injectable()
export class S3UploadService {
  constructor(
    @InjectRepository(Nft)
    private readonly nftRepository: Repository<Nft>,
  ) {}
  async putImageToS3(image: Express.Multer.File, fileName: string) {
    await s3
      .putObject({
        ACL: 'public-read',
        Body: image.buffer,
        Bucket: config.UPLOAD.S3_BUCKET,
        ContentType: image.mimetype,
        Key: fileName,
      })
      .promise();
  }
  
  async handleFileGif(fileNameNotExtension: string , tokenId : string) {
    const fileContent = fs.readFileSync(`public/${fileNameNotExtension}.gif`);
    if (fileContent) {
      await s3.putObject({
        ACL: 'public-read',
        Body: fileContent,
        Bucket: config.UPLOAD.S3_BUCKET,
        ContentType: 'image/gif',
        Key: `${fileNameNotExtension}.gif`,
      }).promise();

      fs.unlinkSync(`public/${fileNameNotExtension}.gif`);

      // fs.unlinkSync(`public/${fileNameNotExtension}.mp4`);
    }
    await this.nftRepository.update({tokenId : tokenId} , {thumbnail : `${config.UPLOAD.S3_DOMAIN}/${fileNameNotExtension}.gif`});
    return true;
  }
}
