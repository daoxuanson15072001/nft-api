import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HolderService } from './holder.service';


@Controller('holder')
export class HolderController {
  constructor(private readonly holderService: HolderService) {}

  @Post('/profile')
  async saveProfileHolder(@Body() body) {
    return this.holderService.saveProfileHolder(body);
  }

  @Get('/:address')
  async getProfileHolder(@Param('address') address) {
    return this.holderService.getProfileHolder(address);
  }
}
