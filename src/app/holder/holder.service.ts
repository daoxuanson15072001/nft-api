import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Holder from 'src/database/entities/Holder';
import { Repository } from 'typeorm';


@Injectable()
export class HolderService {
  constructor(
    @InjectRepository(Holder) private readonly holderRepository: Repository<Holder>,
  ){}

  async saveProfileHolder(params) {
    await this.holderRepository.save(params);
    return true
  }

  async getProfileHolder(address) {
    return await this.holderRepository.findOne({where : {address}});
  }
}
