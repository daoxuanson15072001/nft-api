import { Module } from '@nestjs/common';
import { HolderService } from './holder.service';
import { HolderController } from './holder.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import Holder from 'src/database/entities/Holder';

@Module({
  imports: [TypeOrmModule.forFeature([Holder])],
  controllers: [HolderController],
  providers: [HolderService]
})
export class HolderModule {}
